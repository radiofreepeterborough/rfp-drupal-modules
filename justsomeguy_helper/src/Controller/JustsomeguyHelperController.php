<?php 

/**
 * @file
 * Contains \Drupal\justomeguy_form_helper\Controller\JustsomeguyFormHelperController
 */

namespace Drupal\justsomeguy_helper\Controller;

use Drupal\Core\Controller\ControllerBase;

class JustsomeguyHelperController extends ControllerBase {

  public function content() {

	return array( '#type' => 'markup', '#markup' => 'Hi there I AM CONTENT' );

  }

  public function getTitle() {

	  return  'Contact Me | ' . \Drupal::config()->get('system.site')->get('name');

  }

  

  

}
