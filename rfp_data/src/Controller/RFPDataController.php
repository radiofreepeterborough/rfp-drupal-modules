<?php

namespace Drupal\rfp_data\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Database;
use Drupal\Core\Database\Connection;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class RFPDataController extends ControllerBase {

   private $servername = 'db.mypopupbooks.com';
   private $dbname     = 'tradio_backup_d7';
   private $username   = 'rfnet_db';
   private $password   = 'r0c1t73';
   private $conn 	   = NULL;
   private $mp3_path   = 'http://lcmp.trentradio.ca:17080/';
   private $artists    = array();  // so we don't have to re-look up artist names..
   private $tracks	   = array();  // write out the tracks at the end... 
   private $tracks_by_recording = array();
       

  public function build_json() {
    
	$build = array(  '#type' => 'markup',
      '#markup' => 'BUILD THAT JSON' 
    );
  

  	$this->_init_output_dirs();
	$this->_build_json(); 

	return $build;
  }  

  public function _init_output_dirs() {


  		$dirs =  array(

  			// Artists
  			DRUPAL_ROOT . '/sites/' . \Drupal::request()->getHost() 
  				.'/data/artists/',

  			// Recordings
			DRUPAL_ROOT . '/sites/' . \Drupal::request()->getHost() 
  				.'/data/recordings/',

  			DRUPAL_ROOT . '/sites/' . \Drupal::request()->getHost() 
  				.'/data/recordings/by/',

			DRUPAL_ROOT . '/sites/' . \Drupal::request()->getHost() 
  				.'/data/recordings/by/id/',

  			DRUPAL_ROOT . '/sites/' . \Drupal::request()->getHost() 
  				.'/data/recordings/by/artist/',

  			DRUPAL_ROOT . '/sites/' . \Drupal::request()->getHost() 
  				.'/data/tracks/',

  			DRUPAL_ROOT . '/sites/' . \Drupal::request()->getHost() 
  				.'/data/tracks/by/',

  			DRUPAL_ROOT . '/sites/' . \Drupal::request()->getHost() 
  				.'/data/tracks/by/recording/',
  		);

  		foreach( $dirs as $dir ) {

  			if( !file_exists($dir)) {

  				mkdir($dir);
  			}
  		}



  }


  public function _init_db() {
      
    $this->conn = mysqli_connect( $this->servername, $this->username, $this->password, $this->dbname );
      
  }
    
  public function _build_json( $types = NULL ) {

      $this->conn = mysqli_connect( $this->servername, $this->username, $this->password, $this->dbname );
		
	  if( $types == NULL ) {
		  drupal_set_message('running build for all');
		  $this->_build_artists_json();
		  $this->_build_all_tracks_json();
		  $this->_build_all_recordings_json();
	  }
  }

  public function _build_artists_json(  ) {

		 $artists = array();
		 $recordings = array(); // to manage counting as we go..

	     $this->conn = mysqli_connect( $this->servername, $this->username, $this->password, $this->dbname );
	     
		// drupal_set_message('do we want to record their bio as well?' );
		 $res  = $this->conn->query("SELECT title, nid FROM node WHERE type='artist' ORDER BY title");

		  while( $row = $res->fetch_assoc()) {

		  	// Does this artist have any recordings?
		  	$has_recs_res = $this->conn->query("SELECT count(node.nid) as track_count FROM `field_data_field_artists` 

    LEFT JOIN node ON node.nid = field_data_field_artists.entity_id
    LEFT JOIN field_data_field_album_art ON field_data_field_album_art.entity_id = field_data_field_artists.entity_id 
    LEFT JOIN file_managed ON file_managed.fid = field_data_field_album_art.field_album_art_fid
    LEFT JOIN field_data_field_year_of_release 
        ON field_data_field_year_of_release.entity_id = field_data_field_artists.entity_id 

        WHERE `field_artists_target_id` = '{$row['nid']}' 
        AND field_data_field_artists.bundle='recording' " );

		  		$counter = $has_recs_res->fetch_assoc();
		  		$count = 0;

		  		while( $countrow = $has_recs_res->fetch_assoc()) {

		  			//print "COUNTROW " . $countrow; exit();

		  			if( $countrow->track_count == 0 ) { continue;  }
		  			else { 
		  				$count = $countrow->track_count;
		  				$row['recording_count'] = $countrow->track_count; 
		  				//drupal_set_message("SEE {$row['recording_count']} recordings for {$row['title']} ");
		  			}
		  		}

		  		drupal_set_message('see count:' + $count);

			  $this->artists[ $row['nid']] =  utf8_decode( $row['title'] );

			  $row['name'] = utf8_decode( $row['title'] );

			 
			  unset( $row['title'] );

			  $a =  (object) $row;

			  if( $counter['track_count'] > 0 ) {  // only want artists with recordings

			  	$recordings_count = $this->conn->query("SELECT count(entity_id) as recording_count FROM field_data_field_artists WHERE field_artists_target_id= {$row['nid']}");

			  	$rec_count = $recordings_count->fetch_assoc();


			  	// GRAB the artist recording count from : field_data_field_artists where field_artists_target_id = nid
			  	/*
$has_recs_res = $this->conn->query("SELECT count(node.nid) as track_count FROM `field_data_field_artists` 
			  	*/





			  		$a->track_count = $rec_count['recording_count'];
			  		$a->recording_count = $counter['track_count'];
			  		//$a->recordings = $recordings;

			  		array_push( $artists, $a );
				}

			  //$a->generated = date( "d/M/Y H:i:s", time());
			  
			  $artist_file = DRUPAL_ROOT . '/sites/' . \Drupal::request()->getHost() .'/data/artists/' .$row['nid'].'.json';
			  file_put_contents( $artist_file, json_encode( $a ));
			  unset( $a->generated ); // so it doesn't get repeatedly written to artsists.json as well
              
              // Write recordings
              $this->_build_recordings_for_artist_json( $row['nid'] ); 
		  }
		  
		  $target_file = DRUPAL_ROOT . '/sites/' . \Drupal::request()->getHost() .'/data/artists.json';
	  
		  $response = (object) array(); 

		  $response->artists = $artists;
 		 // $response->generated = date( "d/M/Y H:i:s", time());
		  $json = json_encode( $response );
	
		  $success = file_put_contents( $target_file, json_encode( $response ), LOCK_EX );
 				
		  drupal_set_message('All Artists - built ' . count( $artists ) . ' artists' ); 
  }
    
public function _build_recordings_for_artist_json( $artist_nid ) {
   
    $count = 0;

    $all_tracks_for_artist = array();


    if( !$artist_nid || !is_numeric( $artist_nid ))  return null;
    $this->conn = mysqli_connect( $this->servername, $this->username, $this->password, $this->dbname );
    $res = $this->conn->query("SELECT * FROM `field_data_field_artists` 

    LEFT JOIN node ON node.nid = field_data_field_artists.entity_id
    LEFT JOIN field_data_field_album_art ON field_data_field_album_art.entity_id = field_data_field_artists.entity_id 
    LEFT JOIN file_managed ON file_managed.fid = field_data_field_album_art.field_album_art_fid
    LEFT JOIN field_data_field_year_of_release 
        ON field_data_field_year_of_release.entity_id = field_data_field_artists.entity_id 

        WHERE `field_artists_target_id` = '$artist_nid' 
        AND field_data_field_artists.bundle='recording' " );
   
    while( $row = $res->fetch_assoc() ) {
        
        ++$count;
        $art_stub = $row['filename'];
        if( $art_stub == '' ) $art_stub = 'default_images/NOART.jpg';
     
		$art_url = '/sites/new.radiofreepeterborough.ca/files/covers/'. $art_stub;
        $year_of_release = $row['field_year_of_release_value']; 

       // drupal_set_message('Got REcording ' .$row['nid'] . ' -  ' . $row['title']  . ' for artist nid ' . $artist_nid . ' art: ' . $art_url );    
    
		$recording = array(
		  'nid' => $row['nid'],
		  'title' =>  utf8_decode($row['title']),
		  'cover' => $art_url,
		  'year' => $year_of_release,
		  'artist_nid' => $artist_nid,
		  'artist_name' => $this->artists[$artist_nid]
		);
		 
        $target_path = DRUPAL_ROOT . '/sites/' . \Drupal::request()->getHost() .'/data/recordings/by/artist/' . $artist_nid;
		$target_file = DRUPAL_ROOT . '/sites/' . \Drupal::request()->getHost() .'/data/recordings/by/artist/' 
			. $artist_nid . '/' . $row['nid'] .'.json';  

		$_tracks = $this->_tracks_for_recording( $recording );
		$recording['tracks'] = $_tracks;

		if( !is_array( $_tracks) || empty( $_tracks )) { 

			drupal_set_message("No tracks found for recording {$row['nid']} - skipping " . utf8_decode($row['title']));
			continue; 

		} 

		if( !file_exists( $target_path )) mkdir( $target_path );
		file_put_contents( $target_file, json_encode( (object) $recording ));

		

		

		// and a record for the recording by id..
		$target_file = DRUPAL_ROOT . '/sites/' . \Drupal::request()->getHost() .'/data/recordings/by/id/'. $recording['nid'] .'.json';
		file_put_contents( $target_file, json_encode( (object) $recording ));



       // $_tracks = $this->_tracks_for_recording( $recording );    
        $all_tracks_for_artist[]= $_tracks;
    }

    if( $count == 0 ) {   // this artist has no recordings - remove the json..

    	//print "ARTIST $artist_nid HAS NO RECORDINGS! DELETE THEM FROM "

    }

    $artist_tracks = array();

    foreach( $all_tracks_for_artist as $rec_index  => $recording ) {

   		foreach( $recording  as $i => $track ) { 
    		$artist_tracks[] = $track;
    	}
   	}
    		//exit();

    

    // And finally a record that contains all tracks for any aritst
    // and a record for the recording by id..
	$target_file = DRUPAL_ROOT . '/sites/' . \Drupal::request()->getHost() .'/data/recordings/by/artist/'. $artist_nid .'.json';
		file_put_contents( $target_file, json_encode( (object) 
			$artist_tracks ));
			//$all_tracks_for_artist));
  }
 
  public function _tracks_for_recording( $recording ) {
   
	  $tracks = array();
 $this->conn = mysqli_connect( $this->servername, $this->username, $this->password, $this->dbname );
	  $res = $this->conn->query("SELECT node.nid, node.title, field_data_field_mp3.field_mp3_value as mp3, field_data_field_artists.field_artists_target_id as artist_nid, field_data_field_track_number.field_track_number_value
	 
		FROM  field_data_field_albums 
		LEFT JOIN node ON node.nid = field_data_field_albums.entity_id 
		LEFT JOIN field_data_field_mp3  ON field_data_field_mp3.entity_id = node.nid
		LEFT JOIN field_data_field_artists ON field_data_field_artists.entity_id = node.nid
		LEFT JOIN field_data_field_track_number ON field_data_field_track_number.entity_id = node.nid
		
		WHERE  field_albums_target_id = {$recording['nid']} GROUP BY node.nid  
		ORDER BY field_data_field_track_number.field_track_number_value");

	while( $t = $res->fetch_assoc()) {

			if( !$t['mp3'] ) continue;

			$track = array(
				'nid' => $t['nid'],
				'title' => utf8_decode( $t['title'] ),
				'track_number' => $t['field_track_number_value'],
				'mp3' => $this->mp3_path . $t['mp3'],
				'recording' => $recording['nid'],
				'recording_title' => utf8_decode( $recording['title'] ),
				'recording_cover' => $recording['cover'],
				'artist_nid' => $recording['artist_nid'],
				'artist_name' => utf8_decode( $this->artists[$recording['artist_nid']] )
			);

			array_push( $tracks, $track );
			array_push( $this->tracks, $track );
	}

	//$tracks['generated'] = date( "d/M/Y H:i:s", time());

	$target_path = DRUPAL_ROOT . '/sites/' . \Drupal::request()->getHost() .'/data/tracks/by/recording/' . $recording['nid'].'.json';
	
	//drupal_set_message('writing tracks for ' . $recording['title'] . ' to ' . $target_path );
	file_put_contents( $target_path, json_encode( (object) $tracks ));  
      
    return $tracks;  
  }

  public function _build_all_tracks_json() {


	  $counter = 0;
	  foreach( $this->tracks as $track ) {
		  
		  $target_path = DRUPAL_ROOT . '/sites/' . \Drupal::request()->getHost() .'/data/tracks/' . $track['nid'].'.json'; 
          file_put_contents( $target_path, json_encode( (object) $track ));
		  ++$counter;


	  }

      $target_path = DRUPAL_ROOT . '/sites/' . \Drupal::request()->getHost() .'/data/tracks.json';
      
	  drupal_set_message("Writing data for $counter tracks to tracks.json" );
     // drupal_set_message("TRACKS: " . $this->tracks  . ' with ' . count( $this->tracks ) . ' recs - now write it to '   );
      file_put_contents( $target_path, json_encode( (object)$this->tracks ));
  } 


  public function _build_all_recordings_json() {

	$recordings = array();

 	$this->conn = mysqli_connect( $this->servername, $this->username, $this->password, $this->dbname );
	$res  = $this->conn->query("SELECT title, nid FROM node WHERE type='artist' ORDER BY title");

	while( $row = $res->fetch_assoc()) {
		
		  $artist_nid = $row['nid'];
		  $artist_name = $row['title'];

		  //drupal_set_message("[ ALLREC ] lets get recordings by $artist_name");

		  //print "build json for recordings by $artist_name (nid $artist_nid)<br>";
		  
		  $recording_res = $this->conn->query("SELECT * FROM `field_data_field_artists` 

    LEFT JOIN node ON node.nid = field_data_field_artists.entity_id
    LEFT JOIN field_data_field_album_art ON field_data_field_album_art.entity_id = field_data_field_artists.entity_id 
    LEFT JOIN file_managed ON file_managed.fid = field_data_field_album_art.field_album_art_fid
    LEFT JOIN field_data_field_year_of_release 
        ON field_data_field_year_of_release.entity_id = field_data_field_artists.entity_id 

        WHERE `field_artists_target_id` = '$artist_nid' 
        AND field_data_field_artists.bundle='recording' " );
   
    while( $recording_row = $recording_res->fetch_assoc() ) {
        
        $art_stub = $recording_row['filename'];
        if( $art_stub == '' ) $art_stub = 'default_images/NOART.jpg';
     
		$art_url = '/sites/' . \Drupal::request()->getHost() . '/files/covers/'. $art_stub;
        $year_of_release = $recording_row['field_year_of_release_value']; 
       
		$recording = array(
		  'nid' => $recording_row['nid'],
		  'title' =>  utf8_decode($recording_row['title']),
		  'cover' => $art_url,
		  'year' => $year_of_release,
		  'artist_nid' => $artist_nid,
		  'artist_name' => utf8_decode($artist_name)
		);

		$_tracks = $this->_tracks_for_recording( $recording );
		if( !is_array($_tracks) || empty( $_tracks )) {
			continue; // do not keep recordings that do not have tracks..
		}

		$recording['track_count'] = count( $_tracks ); 
		//drupal_set_message("TODO: Before pushing the recording onto the master stack, check to see if it has any tracks - ALSO - build all storage dirs if they do not exist (recordings/by/artist, etc)");
		
		array_push( $recordings, $recording );
	  }
    }

    // sort the recordings by title
    $didit = usort( $recordings, function( $a, $b ) { 
     		return strcmp( $a['title'], $b['title'] ); 
    });

    
	 $target_path = DRUPAL_ROOT . '/sites/' . \Drupal::request()->getHost() .'/data/recordings.json';
	 file_put_contents( $target_path, json_encode( $recordings ));
  }
   

public function rec_name_cmp( $a, $b ) {

	return strcmp($a->title, $b->title);
}



public function random_tracks( $count ) {
    
    if( !is_numeric( $count )) return null; 
    
    $this->_init_db();
    
   // print_r($this->conn );
    
    
    $build = array(  '#type' => 'markup',
      '#markup' => 'GIVE ME SOME RANDOM TRACKS: ' . $count  . '  of them' 
    );
    


    $tracks = array();
    
    /*
    $res = $this->conn->query("SELECT node.nid, node.title, field_data_field_mp3.field_mp3_value as mp3, field_data_field_artists.field_artists_target_id as artist_nid, field_data_field_track_number.field_track_number_value,
    field_albums_target_id, filename, 

	 
    */
    $res = $this->conn->query("SELECT node.nid, node.title, field_data_field_mp3.field_mp3_value as mp3,
    field_data_field_artists.field_artists_target_id as artist_nid, field_data_field_track_number.field_track_number_value,
    field_albums_target_id, field_album_art_fid,  filename
	 
		FROM  field_data_field_albums 
        
		LEFT JOIN node ON node.nid = field_data_field_albums.entity_id 
		LEFT JOIN field_data_field_mp3  ON field_data_field_mp3.entity_id = node.nid
		LEFT JOIN field_data_field_artists ON field_data_field_artists.entity_id = node.nid
		LEFT JOIN field_data_field_track_number ON field_data_field_track_number.entity_id = node.nid
        LEFT JOIN field_data_field_album_art ON field_data_field_album_art.entity_id = field_albums_target_id
        LEFT JOIN file_managed ON file_managed.fid = field_data_field_album_art.field_album_art_fid
        LEFT JOIN field_data_field_year_of_release 
        ON field_data_field_year_of_release.entity_id = field_data_field_artists.entity_id 
        	
		GROUP BY node.nid   ORDER BY RAND() LIMIT {$count}
        ");

   
	while( $t = $res->fetch_assoc()) {

			if( !$t['mp3'] ) continue;
           
            // Grab the artist's name 
            $artist_res = $this->conn->query("SELECT title FROM node WHERE nid='{$t['artist_nid']}'")->fetch_assoc();
            $artist_name = $artist_res['title'];
        
            // And the recording name
            $recording_res = $this->conn->query("SELECT title FROM node WHERE nid='{$t['field_albums_target_id']}'")->fetch_assoc();
            $recording_name = $recording_res['title'];
        
            $t['artist_name'] = $artist_name;
            
            $art_stub = $t['filename'];
            if( $art_stub == '' ) $art_stub = 'default_images/NOART.jpg';
     
		    $art_url = '/sites/' . \Drupal::request()->getHost() . '/files/covers/'. $art_stub;
    
			$title = utf8_decode( $t['title'] );
			//$title_char_limit = 47;
			//if( strlen( $title ) > $title_char_limit ) $title = substr( $title, 0, $title_char_limit ) . '...';
			

			$track = array(
				'nid' => $t['nid'],
				'title' => $title,
				'track_number' => $t['field_track_number_value'],
				'mp3' => $this->mp3_path . $t['mp3'],
				'recording' => $t['field_albums_target_id'],
				'recording_title' => utf8_decode( $recording_name ),
				'recording_cover' => $art_url,
				'artist_nid' => $t['artist_nid'],
				'artist_name' => utf8_decode( $artist_name )
			);

			array_push( $tracks, $track );
	}

    header("Access-Control-Allow-Origin: *");
    print json_encode( $tracks );
    exit();   
}


 public function artist( $id ) {

	// Given an artist, build a full queue of all their tracks 
	$dir  = $_SERVER['DOCUMENT_ROOT']. '/sites/' . \Drupal::request()->getHost() .'/data/recordings/by/artist/' . $id;

	drupal_set_message("Looking for artist data in " . $dir );

	if( !file_exists( $dir )) { print json_encode( array('JSON FILE NOT FOUND:' . $dir) ); exit(); } 


  $dh = opendir( $dir );
  $artist_tracks = array();
  while ( $f = readdir( $dh )) {

	  if( substr( $f, 0, 1 ) == '.' ) continue;
	  $file = $dir .'/'. $f;
	  $data =  json_decode( file_get_contents( $dir.'/'.$f ), TRUE );
	  $recording_nid = $data['nid'];
	  $tracks_file = $_SERVER['DOCUMENT_ROOT']. '/sites/' 
		  . \Drupal::request()->getHost() .'/data/tracks/by/recording/' . $data['nid'] .'.json';

	  $tracks_json = json_decode( file_get_contents( $tracks_file ) , TRUE );

	  foreach( $tracks_json as $track ) {
			
			if( !is_array( $track ))  continue;
			array_push( $artist_tracks, $track ) ;
	  } 
  }



  header("Access-Control-Allow-Origin: *");

  print json_encode( $artist_tracks );
  exit();

}
        
} // end class



?>
