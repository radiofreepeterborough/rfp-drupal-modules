<?php
/**
 * @file
 * Contains \Drupal\rfp_search\Controller\Rfp_searchStartController .
 */
namespace Drupal\rfp_search\Controller;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Database;

class Rfp_searchStartController {

	public function search( $mode, $keywords_raw, $page ) {

	\Drupal\Core\Database\Database::setActiveConnection('tradio_lcmp');

	$res = null;
	$results = array();

	switch( $mode ) {


		case 'artist':

			 $res = db_select( 'node', 'n' )
				->fields( 'n', array( 'nid', 'type', 'title' ))
				->condition( 'type', 'artist' )
				->condition( 'title', '%' .$keywords_raw .'%', 'LIKE' )
				->execute();

			foreach( $res as $artist ) {
			  $artist->title = utf8_encode( $artist->title );
			  array_push( $results, $artist );
			}

		  break;

		case 'recording':

			$res = db_select( 'node', 'n' )
			  ->fields( 'n', array( 'nid', 'type', 'title' ))
			  ->condition( 'type', 'recording' )
			  ->condition( 'title', '%' .$keywords_raw .'%', 'LIKE' )
			  ->execute();

			foreach( $res as $recording ) {
			  $recording->title = utf8_encode( $recording->title );

			  // Get the JSON file for this recording...
			  $target_file = DRUPAL_ROOT . '/sites/' . \Drupal::request()->getHost()
				  .'/data/recordings/by/id/' . $recording->nid . '.json';
			  $data =  json_decode( file_get_contents( $target_file ));

			  array_push( $results, $data );
			}

		  break;


		case 'track':
			$res = db_select( 'node', 'n' )
				->fields( 'n', array( 'nid', 'type', 'title' ))
				->condition( 'type', 'track' )
				->condition( 'title', '%' .$keywords_raw .'%', 'LIKE' )
				->execute();

				foreach( $res as $track ) {

						$target_file =  DRUPAL_ROOT . '/sites/' . \Drupal::request()->getHost()
						  .'/data/tracks/' . $track->nid . '.json';
					  $data =  json_decode( file_get_contents( $target_file ));

					  array_push( $results, $data );
				}



		break;



	}










	//print 'ok search moo - ' . $mode . ' for ' . $keywords_raw . ' page ' . $page . ' in db ' . $has_connection;

	 // Switch back
	\Drupal\Core\Database\Database::setActiveConnection();
	  print json_encode( $results );


		exit();
	}
}
